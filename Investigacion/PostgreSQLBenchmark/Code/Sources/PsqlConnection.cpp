#include <PsqlConnection.hpp>

#include <iostream>

#include <pqxx/except.hxx>

using namespace PsqlTypes;


namespace Network
{
	PsqlConnection::PsqlConnection()
	{
	}


	PsqlConnection::PsqlConnection(ConnectionInfo connection_info)
		: connection_info(connection_info)
	{
		this->buildConnectionString();
		this->initConnection();
	}


	PsqlConnection::PsqlConnection(std::string host, std::string port, std::string database, std::string user, std::string password)
	{
		this->connection_info.host = host;
		this->connection_info.port = port;
		this->connection_info.database = database;
		this->connection_info.user = user;
		this->connection_info.password = password;

		this->buildConnectionString();
		this->initConnection();
	}


	PsqlConnection::~PsqlConnection()
	{

	}


	std::shared_ptr<pqxx::connection> PsqlConnection::getConnection()
	{
		return this->connection;
	}


	PsqlTypes::ConnectionInfo & PsqlConnection::getConnectionInfo()
	{
		return this->connection_info;
	}


	void PsqlConnection::buildConnectionString()
	{
		this->connection_string += std::string("postgres://") += this->connection_info.user += std::string(":")
			+= this->connection_info.password += std::string("@") += this->connection_info.host += std::string(":")
			+= this->connection_info.port += std::string("/") += this->connection_info.database;

		std::cout << "Built connection string: " << this->connection_string << std::endl;
	}


	void PsqlConnection::initConnection()
	{
		try
		{
			this->connection = std::make_shared<pqxx::connection>(this->connection_string);
		}
		
		catch (pqxx::pqxx_exception &e)
		{
			std::cout << e.base().what() << std::endl; 
		}

		std::cout << "Success!" << std::endl;
	}
}