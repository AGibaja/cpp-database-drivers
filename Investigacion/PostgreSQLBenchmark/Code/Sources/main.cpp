#include <iostream>

#include <pqxx/pqxx>

#include <Command.hpp>
#include <PsqlConnection.hpp>
#include <PostgreCommand.hpp>


using namespace BenchmarkCore;



/// Query employees from database, print results.
int main(int, char *argv[])
{
	Network::PsqlConnection p("192.168.1.49", "5433", "test_db", "alvaro", "070995");

	PsqlCommands::PsqlCommand command(p, "SELECT * FROM test;");
	command.process();

	//try
	//{
	//	pqxx::result r = query();

	//	// Results can be accessed and iterated again.  Even after the connection
	//	// has been closed.
	//	for (auto row : r)
	//	{
	//		std::cout << "Row: ";
	//		// Iterate over fields in a row.
	//		for (auto field : row) std::cout << field;
	//		std::cout << std::endl;
	//	}
	//}
	//catch (const pqxx::sql_error &e)
	//{
	//	std::cerr << "SQL error: " << e.what() << std::endl;
	//	std::cerr << "Query was: " << e.query() << std::endl;
	//	std::cin.get();

	//	return 2;
	//}
	//catch (const std::exception &e)
	//{
	//	std::cerr << "Error: " << e.what() << std::endl;
	//	std::cin.get();

	//	return 1;
	//}

	
	std::cin.get(); 
}