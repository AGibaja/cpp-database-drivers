#include <PqxxCompleteCommand.hpp>


namespace PsqlCommands
{
	PqxxCompleteCommand::PqxxCompleteCommand(Network::PsqlConnection & conn, std::shared_ptr<PsqlCommands::PsqlCommand> command)
		: connection(conn), command(command)
	{
	}


	void PqxxCompleteCommand::init()
	{
		this->command->init();
	}


	void PqxxCompleteCommand::process()
	{
		this->command->process();
	}


	void PqxxCompleteCommand::end()
	{
		this->command->end();
	}


	std::shared_ptr<PsqlCommands::PsqlCommand> PqxxCompleteCommand::getCommand()
	{
		return this->command;
	}

}