#include <PostgreCommand.hpp>

#include <iostream>


namespace PsqlCommands
{
	PsqlCommand::PsqlCommand()
	{
	}


	PsqlCommand::PsqlCommand(Network::PsqlConnection &psql_connection, std::string command)
		: psql_connection(psql_connection), command(command)
	{
		this->init();
	}


	void PsqlCommand::init()
	{
		this->initInternal();
	}


	void PsqlCommand::process()
	{
		this->result = this->work->exec(this->command);
	}


	void PsqlCommand::end()
	{
	}


	std::string & PsqlCommand::getCommandString()
	{
		return this->command;
	}


	pqxx::result & PsqlCommand::getResult()
	{
		return this->result;
	}


	void PsqlCommand::initInternal()
	{
		this->work = std::make_shared<pqxx::work>(*this->psql_connection.getConnection());
	}
}