#ifndef PSQLTYPES_HPP
#define PSQLTYPES_HPP

#include <string>

namespace PsqlTypes
{
	struct ConnectionInfo
	{
		std::string host;
		std::string port;
		std::string database;
		std::string user;
		std::string password;
	};
}


#endif // !define PSQLTYPES_HPP
