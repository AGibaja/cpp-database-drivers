#ifndef POSTGRECOMMAND_HPP
#define POSTGRECOMMAND_HPP

#include <PsqlConnection.hpp>

#include <Command.hpp>

#include <pqxx/pqxx>


namespace PsqlCommands
{
	class PsqlCommand : public BenchmarkCore::Command
	{
	public:
		PsqlCommand();
		PsqlCommand(Network::PsqlConnection &psql_connection, std::string command);

		void init() override;
		void process() override;
		void end() override;

		std::string &getCommandString();

		pqxx::result &getResult();


	private:
		Network::PsqlConnection psql_connection;

		std::string command; 

		std::shared_ptr<pqxx::work> work = nullptr; 
		
		pqxx::result result;


		void initInternal(); 


	};


}

#endif // !POSTGRECOMMAND_HPP