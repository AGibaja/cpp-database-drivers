#ifndef PSQLCONNECTION_HPP
#define PSQLCONNECTION_HPP

#include <string>
#include <memory>

#include <pqxx/connection.hxx>

#include <PsqlTypes.hpp>


namespace Network
{
	class PsqlConnection
	{
	public:
		PsqlConnection();
		PsqlConnection(PsqlTypes::ConnectionInfo connection_info);
		PsqlConnection(std::string host, std::string port, std::string database,
			std::string user, std::string password);
		~PsqlConnection();

		std::shared_ptr<pqxx::connection> getConnection();

		PsqlTypes::ConnectionInfo &getConnectionInfo(); 


	private:
		std::string connection_string; 

		std::shared_ptr<pqxx::connection> connection = nullptr;

		PsqlTypes::ConnectionInfo connection_info;


		void buildConnectionString();
		void initConnection();


	};

}


#endif // !POSTGRESQLCONNECTION_HPP
