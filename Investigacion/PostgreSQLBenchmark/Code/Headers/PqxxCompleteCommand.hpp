/*!
@class PqxxCompleteCommand
@brief Class that encapsulate pqxx connections and pqxx commands.
*/


#ifndef PQXXCOMPLETECOMMAND_HPP
#define PQXXCOMPLETECOMMAND_HPP

#include <PostgreCommand.hpp>


namespace PsqlCommands
{
	class PqxxCompleteCommand : public BenchmarkCore::Command
	{
	public:
		PqxxCompleteCommand(Network::PsqlConnection &conn, std::shared_ptr<PsqlCommands::PsqlCommand> command);

		///Initialize the contained psql command.
		void init() override;
		///Process the contained psql command.
		void process() override;
		///End the contained psql command.
		void end() override;

		///Get a pointer the psql command.
		std::shared_ptr <PsqlCommands::PsqlCommand> getCommand();


	private:
		Network::PsqlConnection connection;

		std::shared_ptr<PsqlCommands::PsqlCommand> command = nullptr;


	};
}


#endif // !define PQXXCOMPLETECOMMAND_HPP
