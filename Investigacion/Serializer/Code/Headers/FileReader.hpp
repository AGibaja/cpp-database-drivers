#ifndef FILEREADER_HPP
#define FILEREADER_HPP

#include <string>
#include <fstream>


namespace OwnIO
{
	class FileReader
	{
	public:
		FileReader(std::string path);

		std::string &getFileAsString();

		size_t getFileLength();


	private:
		size_t file_length;

		std::string path;
		std::string buffer;

		std::ifstream file; 

		void readFile();

	};
}

#endif // !define FILEREADER_HPP
