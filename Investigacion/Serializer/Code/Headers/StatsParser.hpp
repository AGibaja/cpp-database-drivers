#ifndef PARSER_HPP
#define PARSER_HPP

#include <memory>
#include <map>

#include <rapidjson/document.h>

#include <FileReader.hpp>


namespace OwnParser
{
	class StatsParser
	{
	public:
		StatsParser(const std::string path);

		bool hasSuffix(const std::string &str, const std::string &suffix);

		const char *getTitle();

		rapidjson::Value &getXArray();
		rapidjson::Value &getYArray();

		std::map<int, int> getXYMap();


	private:
		const std::string path; 

		const char *title;

		rapidjson::Value x_array;
		rapidjson::Value y_array;

		rapidjson::Document document;

		std::shared_ptr<OwnIO::FileReader> reader = nullptr;

		bool setTitle();
		bool setXArray();
		bool setYArray();
		
	};
}

#endif // !define PARSER_HPP
