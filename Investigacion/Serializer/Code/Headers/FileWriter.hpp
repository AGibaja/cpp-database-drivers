#ifndef FILEWRITER_HPP
#define FILEWRITER_HPP

#include <iostream>
#include <string>
#include <fstream>


namespace OwnIO
{
	class FileWriter
	{
	public:
		FileWriter(std::string path, bool create_not_exists = false);

		void writeString(std::string &str);


	private:
		bool create_not_exists;

		std::string path; 

		std::ofstream ifile;


	};
}

#endif // !define FILEWRITER_HPP
