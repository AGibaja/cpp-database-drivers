#include <map>
#include <string>
#include <iostream>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>


class Stats
{
public:
	Stats(std::string main_title, std::string x_title, std::string y_title, std::map<int, int> map);

	const std::string getAsString();


private:
	std::string main_title, x_title, y_title = "Unnamed";
	
	std::map<int, int> map;

	rapidjson::Document doc;

	rapidjson::StringBuffer str_buff;


	void initTitle();
	void initXTitle();
	void initYTitle();
	void dump();
	void addMember(rapidjson::Value &a, rapidjson::Value &b);


};