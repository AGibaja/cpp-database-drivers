#include <StatsParser.hpp>

#include <cassert>
#include <iostream>
#include <vector>


using namespace rapidjson;


namespace OwnParser
{
	StatsParser::StatsParser(const std::string path)
		: path(path)
	{
		this->reader = std::make_shared<OwnIO::FileReader>(path);

		if (this->hasSuffix(this->path, ".json"))
		{
			ParseResult result = this->document.Parse(this->reader->getFileAsString().c_str());

			if (result.IsError())
			{
				assert(result && "Error in parsing. Returning");
				return;
			}

			this->setTitle();
			this->setXArray();
			this->setYArray();
			this->getXYMap();
		}

		else
			assert(1 != 1 && "Error");	
	}


	bool StatsParser::hasSuffix(const std::string &str, const std::string &suffix)
	{
		std::cout << "Is: " << str.c_str() << std::endl;
		return str.size() >= suffix.size() &&
			str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
	}


	const char *StatsParser::getTitle()
	{
		return this->title;
	}


	rapidjson::Value &StatsParser::getXArray()
	{
		return this->x_array;
	}


	rapidjson::Value &StatsParser::getYArray()
	{
		return this->y_array;
	}


	std::map <int, int> StatsParser::getXYMap()
	{
		
		std::map<int, int> m;

		std::pair<int, int> p;

		int i = 0;
		for (Value &x : this->x_array.GetArray())
		{
			p.first = x.GetInt();
			p.second = this->y_array.GetArray()[i].GetInt();
			m.insert(p);
			++i;
		}

		return m;
	}


	bool StatsParser::setTitle()
	{
		this->title = this->document["Title"].GetString();
		return true;
	}


	bool StatsParser::setXArray()
	{
		this->x_array = this->document["XAxis"].GetArray();
		return true;
	}


	bool StatsParser::setYArray()
	{
		this->y_array = this->document["YAxis"].GetArray();
		return true;
	}

}