#include <Stats.hpp>


using namespace rapidjson;


Stats::Stats(std::string main_title, std::string x_title, std::string y_title, std::map<int, int> map)
	: main_title(main_title), x_title(x_title), y_title(y_title), map(map)
{
	doc.SetObject();
	this->initTitle();
	this->initXTitle();
	this->initYTitle();
	this->dump();
}


const std::string Stats::getAsString()
{
	return this->str_buff.GetString();
}


void Stats::initTitle()
{
	Document::AllocatorType &alloc = doc.GetAllocator();

	Value title_name(kStringType);
	title_name.SetString("Title", alloc);

	Value title_value(kStringType);
	title_value.SetString(this->main_title.c_str(), alloc);

	this->addMember(title_name, title_value);
}


void Stats::initXTitle()
{
	Document::AllocatorType &alloc = doc.GetAllocator();

	Value x_title(kStringType);
	x_title.SetString(this->x_title.c_str(), alloc);

	Value x_array(kArrayType);
	for (auto &xe : this->map)
	{
		x_array.PushBack(xe.first, alloc);
	}

	this->addMember(x_title, x_array);

}


void Stats::initYTitle()
{
	Document::AllocatorType &alloc = doc.GetAllocator();

	Value y_title(kStringType);
	y_title.SetString(this->y_title.c_str(), alloc);

	Value y_array(kArrayType);
	for (auto &ye : this->map)
	{
		y_array.PushBack(ye.second, alloc);
	}

	this->addMember(y_title, y_array);
}


void Stats::dump()
{
	Writer<StringBuffer> writer(str_buff);
	doc.Accept(writer);
}


void Stats::addMember(Value &a, Value &b)
{
	this->doc.AddMember(a, b, this->doc.GetAllocator());
}