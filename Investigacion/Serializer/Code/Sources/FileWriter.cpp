#include <FileWriter.hpp>

#include <cassert>


using namespace std;


namespace OwnIO
{
	FileWriter::FileWriter(std::string path, bool create_not_exists)
		: path(path), create_not_exists(create_not_exists)
	{
		if (create_not_exists)
			this->ifile = std::ofstream(path);
		else
		{
			this->ifile = std::ofstream(path, ofstream::in);
			if (!this->ifile.is_open())
				assert(this->ifile.is_open());
		}
	}


	void FileWriter::writeString(std::string & str)
	{
		this->ifile << str;
		this->ifile.close();
	}
}