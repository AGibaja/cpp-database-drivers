#include <FileReader.hpp>

#include <iostream>
#include <cassert>


namespace OwnIO
{
	FileReader::FileReader(std::string path)
		: path (path)
	{
		this->file = std::ifstream(path);
		if (!this->file.is_open())
		{
			assert(this->file.is_open() && "Couldn't open file at path: ");
			std::cout << "	" << this->path << std::endl;
		}

		else
		{
			this->file.seekg(0, this->file.end);
			this->file_length = this->file.tellg();
			std::cout << "Length: " << this->file_length << std::endl; 
			this->file.seekg(0, this->file.beg);
			this->readFile(); 
		}
	}


	std::string &FileReader::getFileAsString()
	{
		return this->buffer;
	}


	size_t FileReader::getFileLength()
	{
		return this->file_length;
	}


	void FileReader::readFile()
	{
		this->buffer.resize(this->file_length);
		this->file.read((char*)&this->buffer[0], this->file_length);
	}
}