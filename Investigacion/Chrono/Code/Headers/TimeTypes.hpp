#pragma once
#ifndef TIMETYPES_HPP
#define TIMETYPES_HPP

#include <chrono>


namespace TimeTypes
{
	using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;

	using Nanoseconds = std::chrono::nanoseconds;
	using Microseconds = std::chrono::microseconds;
	using Millisconds = std::chrono::milliseconds;
	using Seconds = std::chrono::seconds;
	using Minutes = std::chrono::minutes;
	using Hours = std::chrono::minutes;
}

#endif // !#define TIMETYPES_HPP