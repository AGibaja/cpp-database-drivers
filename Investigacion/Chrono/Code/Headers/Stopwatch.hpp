#ifndef STOPWATCH_HPP
#define STOPWATCH_HPP

#include <cassert>
#include <iostream>


#include <TimeTypes.hpp>


namespace TimeUtilities
{
	template <class T>
	class Stopwatch
	{
	public:
		Stopwatch()
		{
 			std::cout << "Chrono initialized." << std::endl;
		}


		void setStart()
		{
			if (this->start_time_set)
			{
     			std::cout << "Start time already set. Reseting..." << std::endl;
				this->elapsed = 0; 
				this->start_time_set = true;
				this->start = std::chrono::steady_clock::now();
			}
			else
			{
				this->start_time_set = true; 
				this->start = std::chrono::steady_clock::now();
			}
		}


		void setEnd()
		{
			if (!this->start_time_set)
			{
				assert(this->start_time_set && "Start time has not been set.");
			}
			else
			{
				this->end_time_set = true;
				
				this->end = std::chrono::steady_clock::now();
				this->elapsed = std::chrono::duration_cast<T>(this->end - this->start).count();
				this->elapsed_raw = std::chrono::duration_cast<T>(this->end - this->start);
				std::cout << "Elapsed: " << this->elapsed << std::endl; 
			}
		}


		inline int getElapsed()
		{
			return this->elapsed;
		}


		inline T getElapsedRaw()
		{
			return this->elapsed_raw;
		}


	private:
		bool start_time_set = false;
		bool end_time_set = false;

		TimeTypes::TimePoint start; 
		TimeTypes::TimePoint end;

		int elapsed;

		T elapsed_raw; 

	};
}


#endif // !define CHRONO_HPP
