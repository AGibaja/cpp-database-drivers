#include <iostream>
#include <map>
#include <chrono>

#include <Stopwatch.hpp>
#include <TimeTypes.hpp>


using namespace TimeUtilities; 

static Stopwatch <TimeTypes::Nanoseconds> watch;


void fun()
{
	std::map <int, TimeTypes::Nanoseconds> bench_map;

	for (int i = 0; i < 1000; ++i)
	{
		watch.setStart();
		for (int i = 0; i < 100000; ++i)
			5 / 7.3f;
		watch.setEnd();
		bench_map.insert(std::pair<int, TimeTypes::Nanoseconds>(i, watch.getElapsedRaw()));
	}
	
	std::cout << "Bench mark capactiry is: " << bench_map.size() << std::endl;

	for (auto & e : bench_map)
	{
		std::cout << "Try number: " << e.first << " took: " << e.second.count() << std::endl; 
	}

}


int main()
{
	fun();
	std::cout << "Paused." << std::endl;
	std::cin.get(); 

	return 0; 
}