#ifndef TESTCOMMAND_HPP
#define TESTCOMMAND_HPP

#include <Command.hpp>

#include <ReadCommand.hpp>
#include <iostream>



namespace BenchmarkCore
{
	class TestCommand : public Command
	{
	public:
		TestCommand(DatabaseType::DatabaseType database_type
			= DatabaseType::DatabaseType::TESTCOMMAND);
		~TestCommand();

		void init() override;
		void process() override;
		void end() override;
	};


	class TestReadCommand : public ReadCommand
	{
		void init() override
		{
			std::cout << "Test Read Command." << std::endl; 
		}


		void process() override
		{

		}


		void end() override
		{

		}

	};
}


#endif // !define TESTCOMMAND_HPP
