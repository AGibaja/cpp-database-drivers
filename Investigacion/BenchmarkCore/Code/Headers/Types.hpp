#ifndef TYPES_HPP
#define TYPES_HPP

namespace Types
{
	namespace MeasureUnit
	{
		enum MeasureUnit
		{
			UNDEFINED = 0,
			NANOSECONDS,
			MICROSECONDS,
			MILISECONDS,
			SECONDS,
			MINUTES,
			HOURS
		};
	}
	

	namespace DatabaseType
	{
		enum class DatabaseType
		{
			UNDEFINED = 0,
			POSTGRESQL,
			MONGODB,
			REDIS,
			TESTCOMMAND
		};
	}
	

	namespace CommandType
	{
		enum CommandType
		{
			UNDEFINED = 0,
			READ,
			WRITE
		};
	}
	
}

#endif