#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <Types.hpp>

using namespace Types;

namespace BenchmarkCore
{
	
	class Command
	{
	private:
		///CommandType
		using CType = CommandType::CommandType;
		///DatabaseType
		using DType = DatabaseType::DatabaseType;


	public:
		~Command();

		virtual void init() = 0; 
		virtual void process() = 0; 
		virtual void end() = 0; 

		bool isInited();
		bool isProcessed();
		bool isEnded(); 

		CType getCommandType();
		DType getDatabaseType();


	protected:
		CommandType::CommandType command_type =
			CommandType::CommandType::UNDEFINED;

		DatabaseType::DatabaseType database_type =
			DatabaseType::DatabaseType::UNDEFINED;


		Command(DType database_type = DType::UNDEFINED);

		void isInited(bool inited);
		void isProcessed(bool processed);
		void isEnded(bool ended);


	private: 
		bool inited = false;
		bool processed = false;
		bool ended = false;

		

	};
}

#endif // !COMMAND_HPP
