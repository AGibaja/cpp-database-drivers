#ifndef READCOMMAND_HPP
#define READCOMMAND_HPP

#include <Command.hpp>
#include <Types.hpp>

using namespace Types; 


namespace BenchmarkCore
{
	class ReadCommand : public Command
	{
	public:
		ReadCommand() 
			: Command()
		{
			this->command_type = CommandType::CommandType::READ;
		}

		
		virtual void init() = 0;
		virtual void process() = 0;
		virtual void end() = 0;

	};
}

#endif 
