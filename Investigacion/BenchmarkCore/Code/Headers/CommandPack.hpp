/**
	@class CommandPack
	@brief Class that stores a group of commmands and executes them as required.
	@details
		It expects as a template type the type of command that it will perform.
		For example: CommandPack<DatabaseType::DatabaseType::POSTGRESQL> is a command pack
		that contains multiple POSTGRESQL commands.
*/ 

#ifndef COMMANDPACK_HPP
#define COMMANDPACK_HPP

#include <vector>
#include <memory>
#include <cassert>

#include <iostream>

#include <Command.hpp>


namespace BenchmarkCore
{
	template <typename T>
	class CommandPack : public Command
	{
	public:
		///Only create an empty command pack.
		CommandPack()
		{
			std::cout << "Command pack type constructor called. Empty command pack created." << std::endl;
			assert(this->isValidDatabaseType() && "DatabaseType is invalid.");
		}


		///Initalized all commands from an external list.
		CommandPack(std::vector <std::shared_ptr<Command>> &commands)
			: commands (commands)
		{
			std::cout << "Command pack initialized from a command vector." << std::endl; 
			assert(this->isValidDatabaseType() && "DatabaseType is invalid.");
		}


		~CommandPack()
		{

		}


		///Initialize all commands of the command pack.
		void init() override
		{
			for (auto & c : this->commands)
				c->init();
			std::cout << "Command pack initialized." << std::endl;
		}


		///Process all commands of the command pack.
		void process() override
		{
			for (auto & c : this->commands)
				c->process();
			std::cout << "Command pack processed." << std::endl;
		}


		///End all commands of the command pack.
		void end() override
		{
			for (auto & c : this->commands)
				c->end();
			std::cout << "Command pack ended." << std::endl;
		}


		///This is a command pack. Always return true.
		bool isCommandPack()
		{
			return false;
		}


	private:
		///Check if a CommandPack of the type that is being hold is valid.
		bool isValidDatabaseType()
		{
			using DbType = DatabaseType::DatabaseType;
			
			//Comparamos el tipo del dato almacenado con todos los tipos de bases de datos: 
			std::shared_ptr<T> holden_type = std::make_shared<T>();
			std::cout << "Test cast done." << std::endl; 

			if (std::dynamic_pointer_cast<Command>(holden_type) == nullptr)
			{
				std::cout << "Invalid type held." << std::endl;
				assert(1 != 1 && "Invalid cast to Command type."); 
				return false; 
			}
				
			if (!this->checkChildrenCompatibleTypes())
			{
				assert(1!=1 && "Undefined child.");
				return false;
			}

			else
				return true; 
			
		}


		inline bool checkChildrenCompatibleTypes()
		{
			using DbType = DatabaseType::DatabaseType;
			for (auto &c : this->commands)
			{
				std::cout << "Type is: " << (int)c->getDatabaseType() << std::endl; 
				switch (c->getDatabaseType())
				{
					case (DbType::MONGODB):
					{
						std::cout << "MongoDB type detected in children." << std::endl;
						return true;
					}

					case (DbType::POSTGRESQL):
					{
						std::cout << "PostgreSQL type detected in children." << std::endl;
						return true;
					}

					case (DbType::REDIS):
					{
						std::cout << "Redis type detected in children." << std::endl;
						return true;
					}

					case (DbType::TESTCOMMAND):
					{
						std::cout << "Test type detected in children." << std::endl;
						return true;
					}

					case (DbType::UNDEFINED):
					{
						std::cout << "Undefined type in child. Aborting..." << std::endl;
						return false;
					}

					default:
						return false;
				}
			}
			std::cout << "All good." << std::endl; 
			return true;
		}

		///How many connections do we want to use to process al commands in this pack?
		int connections_count = -1;
		///Time elapsed between when commands start to process and commands are done
		///being processed.
		int total_response_time = -1;

		///Commands that form the command pack.
		std::vector <std::shared_ptr<Command>> commands;

	};

}

#endif // !COMMANDPACK_HPP
