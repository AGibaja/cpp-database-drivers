#ifndef CONNECTIONINFO_HPP
#define CONNECTIONINFO_HPP

#include <string>

namespace Network
{
	class ConnectionInfo
	{
	public:
		struct ConnectionParams
		{
			std::string host;
			std::string database_name;
			std::string user;
			std::string password;

		} connection_params;

		ConnectionInfo();
		ConnectionInfo(std::string &host, std::string &database_name,
			std::string &user, std::string &password);
		ConnectionInfo(ConnectionParams &params);
		
		virtual ~ConnectionInfo();

		std::string &getHost();
		std::string &getDbName();
		std::string &getUserName();
		std::string &getPassword();


	protected:
		virtual void initConnection() = 0; 
		virtual void makeConnection() = 0; 
		virtual void endConnection() = 0; 


	private:
		std::string host = "127.0.0.1";
		std::string database_name = "root";
		std::string user = "root";
		std::string password = "root";

	};
}



#endif