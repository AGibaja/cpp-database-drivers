#ifndef COMMANDINDEX_HPP
#define COMMANDINDEX_HPP

#include <map>
#include <string>
#include <memory>

#include <Command.hpp>

namespace BenchmarkCore
{
	class CommandIndex
	{
	public:
		using CIndex = std::map <std::string, std::shared_ptr<BenchmarkCore::Command>>;

		void addCommand(std::string &id, std::shared_ptr<BenchmarkCore::Command> command);
		void addCommand(std::pair<std::string, std::shared_ptr<BenchmarkCore::Command>> &command);

		static CIndex &getCommandsIndex();


	private:
		CommandIndex();

		static std::map <std::string, std::shared_ptr<BenchmarkCore::Command>> commands_index;


	};

}

#endif // !define COMMANDINDEX_HPP
