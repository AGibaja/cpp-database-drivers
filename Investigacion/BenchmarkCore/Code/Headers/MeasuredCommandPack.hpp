#ifndef MEASUREDCOMMANDPACK_HPP
#define MEASUREDCOMMANDPACK_HPP

#include <CommandPack.hpp>

#include <Stopwatch.hpp>

using namespace TimeUtilities;


namespace BenchmarkCore
{
	///Specify time measurement unit of a given command type T of type X.
	template <typename T, typename X = TimeTypes::Millisconds>
	class MeasuredCommandPack : public CommandPack <T>
	{
	public:
		MeasuredCommandPack()
			: CommandPack<T>()
		{
			std::cout << "Default measure unit: " << typeid(X).name() << std::endl; 
		}


		MeasuredCommandPack(std::vector <std::shared_ptr<Command>> commands)
			: CommandPack<T>(commands)
		{
		}


		virtual ~MeasuredCommandPack()
		{

		}


		void process() override
		{
			this->stopwatch.setStart();
			CommandPack<T>::process(); 
			this->stopwatch.setEnd();
		}


		int getTimeElapsed()
		{
			return this->stopwatch.getElapsed();
		}


		X getTimeElapsedRaw()
		{
			return this->stopwatch->getElapsedRaw();
		}


		Stopwatch<X> &getStopWatch()
		{
			return this->stopwatch;
		}


	private:
		Stopwatch <X> stopwatch;


	};

}

#endif // !define MEASUREDCOMMANDPACK_HPP
