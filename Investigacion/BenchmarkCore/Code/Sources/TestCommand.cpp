#include <TestCommand.hpp>

#include <iostream>



namespace BenchmarkCore
{
	TestCommand::TestCommand(DatabaseType::DatabaseType database_type)
		: Command(database_type)
	{
		std::cout << "Test Command inited." << std::endl; 
		this->init();
		this->process();
		this->end(); 
	}


	TestCommand::~TestCommand()
	{

	}


	void TestCommand::init()
	{
		std::cout << "Test Command initialized." << std::endl;
		this->isInited(true);
	}


	void TestCommand::process()
	{
		std::cout << "Test Command processed." << std::endl; 
		this->isProcessed(true);
	}


	void TestCommand::end()
	{
		std::cout << "Test Command ended." << std::endl;
		this->isInited(true);
	}

}