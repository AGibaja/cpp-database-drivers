#include <ConnectionInfo.hpp>

namespace Network
{
	ConnectionInfo::ConnectionInfo()
	{

	}


	ConnectionInfo::ConnectionInfo(std::string & host, std::string & database_name,
		std::string & user, std::string & password)
		: host(host), database_name(database_name), user(user), password(password)
	{
		this->connection_params.host = host;
		this->connection_params.database_name = database_name;
		this->connection_params.user = user;
		this->connection_params.password = password;
	}


	ConnectionInfo::ConnectionInfo(ConnectionParams & params)
		: connection_params (params)
	{
		this->host = params.host;
		this->database_name = params.database_name;
		this->user = params.user;
		this->password = params.password;
	}


	ConnectionInfo::~ConnectionInfo()
	{

	}


	std::string & ConnectionInfo::getHost()
	{
		return this->host;
	}


	std::string & ConnectionInfo::getDbName()
	{
		return this->database_name;
	}


	std::string & ConnectionInfo::getUserName()
	{
		return this->user;
	}


	std::string & ConnectionInfo::getPassword()
	{
		return this->password;
	}
}