#include <CommandsIndex.hpp>

namespace BenchmarkCore
{
	void CommandIndex::addCommand(std::string & id, std::shared_ptr<Command> command)
	{		
		this->commands_index[id] = command;
	}


	void CommandIndex::addCommand(std::pair<std::string, std::shared_ptr<Command>>& command)
	{
		this->commands_index[command.first] = command.second;
	}


	CommandIndex::CIndex &CommandIndex::getCommandsIndex()
	{
		return commands_index;
	}

}


