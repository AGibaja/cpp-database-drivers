#include <Command.hpp>

#include <iostream>


namespace BenchmarkCore
{
	Command::Command(Command::DType database_type)
		: database_type(database_type)
	{
		std::cout << "Command constructor called. Type assigned is: " << (int)database_type << std::endl; 
	}


	Command::~Command()
	{

	}


	void Command::isInited(bool inited)
	{
		this->inited = inited;
	}


	void Command::isProcessed(bool processed)
	{
		this->processed = processed;
	}


	void Command::isEnded(bool ended)
	{
		this->ended = ended;
	}


	bool Command::isInited()
	{
		return this->inited;
	}


	bool Command::isProcessed()
	{
		return this->processed;
	}


	bool Command::isEnded()
	{
		return this->ended;
	}


	Command::CType Command::getCommandType()
	{
		return this->command_type;
	}


	Command::DType Command::getDatabaseType()
	{
		return this->database_type;
	}
}

