#include <iostream>
#include <memory>

#include <TestCommand.hpp>
#include <CommandPack.hpp>



#include <Types.hpp>


using namespace BenchmarkCore;


int main()
{
	TestReadCommand r; 
	std::cout << "BenchmarkCore module tests started." << std::endl; 
	std::vector <std::shared_ptr<Command>> commands
	{
		std::make_shared<TestCommand>(DatabaseType::DatabaseType::TESTCOMMAND),
		std::make_shared<TestCommand>(DatabaseType::DatabaseType::TESTCOMMAND),
		std::make_shared<TestCommand>(DatabaseType::DatabaseType::TESTCOMMAND),
	};
	
	for (auto c : commands)
	{
		if (c->getDatabaseType() == DatabaseType::DatabaseType::UNDEFINED)
			;
	}

	CommandPack <TestCommand> command_pack (commands); 

	std::cin.get(); 
}