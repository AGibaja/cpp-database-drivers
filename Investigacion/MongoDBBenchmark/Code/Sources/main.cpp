#include <iostream>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/exception/exception.hpp>

#include <MongoConnection.hpp>
#include <MongoFindCommand.hpp>
#include <MongoInsertCommand.hpp>



using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

using namespace MongoCommands;


int main(int, char**)
{
	std::shared_ptr <MongoConnection> connection
		= std::make_shared<MongoConnection>
		("192.168.1.49", "counting",
		"27017", "root", "");


	MongoCommands::MongoFindCommand f = MongoCommands::MongoFindCommand(connection, "entries", "{\"ClassA\" : \"Name\"}");
	//MongoCommands::MongoInsertCommand i = MongoCommands::MongoInsertCommand(connection, "entries", "{data : inserted }");
	//i.process();
	//std::cin.get();

	/*try
	{
		mongocxx::client client{ mongocxx::uri{"mongodb://192.168.1.49:27017/root"} };
		mongocxx::database db = client["counting"];
		mongocxx::collection collection = db["entries"];
		mongocxx::cursor cur = collection.find({});

		for (auto & e : cur)
		{
			bsoncxx::document::view view = e;
			auto stored = view["_id"];
			std::cout << (stored.get_oid().value.to_string()) << std::endl;
		}
	}

	catch (mongocxx::exception &e)
	{
		std::cout << e.what() << std::endl;
		std::cin.get();
		return -1;
	}

	std::cout << "All good." << std::endl;*/
	std::cin.get();
	return 0;
}