#include <MongoConnection.hpp>

#include <mongocxx/instance.hpp>
#include <mongocxx/exception/exception.hpp>
#include <mongocxx/client.hpp>


namespace MongoCommands
{
	MongoConnection::MongoConnection(Network::ConnectionInfo::ConnectionParams &params)
		: ConnectionInfo(params)
	{
	}


	MongoConnection::MongoConnection(std::string host, std::string db_name, 
		std::string port, std::string user, std::string password)
		: ConnectionInfo(host, db_name, user, password), port(port)
	{
	}


	MongoConnection::~MongoConnection()
	{
	}


	void MongoConnection::initConnection()
	{
		if (!this->inited)
		{
			try
			{
				auto str = this->built_connection += this->getHost()
					+= std::string(":") += this->port
					+= std::string("/") += this->getDbName();

				std::cout << "Built connection: " << this->built_connection << std::endl;

				this->client = mongocxx::client{ mongocxx::uri{this->built_connection} };
				this->database = this->client[this->getDbName()];
			}

			catch (mongocxx::exception &e)
			{
				std::cout << "Exception: " << e.what() << std::endl;
			}
			this->inited = true;
		}

		/*
		else
			std::cout << "Trying to init connection twice." << std::endl;*/

	}


	void MongoConnection::makeConnection()
	{
		std::cout << "Make the connection from monngocxx driver and check for excpetions." << std::endl; 
	}


	void MongoConnection::endConnection()
	{
		std::cout << "Clean up connection from mongocxx driver if necessary." << std::endl;

	}


	void MongoConnection::setMongoDatabase(mongocxx::database db)
	{
		this->database = db;
	}


	bool MongoConnection::connectionInited()
	{
		return this->inited;
	}


	std::string & MongoConnection::getBuiltConnection()
	{
		return this->built_connection;
	}


	std::string & MongoConnection::getPort()
	{
		return this->port;
	}


	mongocxx::database MongoConnection::getMongoDatabase()
	{
		return this->database;
	}


}