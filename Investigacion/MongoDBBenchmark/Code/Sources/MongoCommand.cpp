#include <MongoCommand.hpp>

#include <iostream>

#include <Types.hpp>
#include <MongoConnection.hpp>



using namespace Types; 


namespace MongoCommands
{
	MongoCommand::MongoCommand(std::shared_ptr<MongoConnection> conn, std::string collection)
		: parent_connection(conn), collection_name(collection)
	{
		this->database_type = DatabaseType::DatabaseType::MONGODB;
		this->init();
	}


	MongoCommand::~MongoCommand()
	{
	}


	void MongoCommand::init()
	{
		this->getConnection()->initConnection();
		this->mongo_collection = this->getConnection()->getMongoDatabase()[this->collection_name];

	}


	void MongoCommand::process()
	{
	}


	void MongoCommand::end()
	{
	}


	std::shared_ptr<MongoConnection> MongoCommand::getConnection()
	{
		return this->parent_connection;
	}
}


