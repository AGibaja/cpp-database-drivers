#include <MongoCompleteInsertCommand.hpp>


namespace MongoCommands
{
	MongoCompleteInsertCommand::MongoCompleteInsertCommand(std::shared_ptr<MongoConnection> connection, std::shared_ptr<MongoInsertCommand> command)
		: connection(connection), command(command)
	{
	}


	std::shared_ptr<MongoConnection> MongoCompleteInsertCommand::getConnection()
	{
		return this->connection;
	}


	std::shared_ptr<MongoInsertCommand> MongoCompleteInsertCommand::getInsertCommand()
	{
		return this->command;
	}


	void MongoCompleteInsertCommand::init()
	{
		this->command->init();
	}


	void MongoCompleteInsertCommand::process()
	{
		this->command->process();
	}


	void MongoCompleteInsertCommand::end()
	{
		this->command->end();
	}
}
