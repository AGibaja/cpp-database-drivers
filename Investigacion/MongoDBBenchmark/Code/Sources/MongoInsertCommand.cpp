#include <MongoInsertCommand.hpp>

#include <iostream>

#include <bsoncxx/builder/stream/document.hpp>
#include <MongoConnection.hpp>


using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;



namespace MongoCommands
{
	MongoInsertCommand::MongoInsertCommand(std::shared_ptr<MongoConnection> connection,
		std::string collection_name, std::string data_to_insert)
		: MongoCommand(connection, collection_name), data_to_insert(data_to_insert)
	{
		this->init();
	}


	MongoInsertCommand::~MongoInsertCommand()
	{
	}


	void MongoInsertCommand::init()
	{
		for (int i = 0; i < 100; ++i)
		{
			this->docs.push_back(document{} << "Num" << i << finalize);		
		}
	}


	void MongoInsertCommand::process()
	{
		this->mongo_collection.insert_many(this->docs);
	}


	void MongoInsertCommand::end()
	{
		this->docs.clear();
	}
}