#include <MongoCompleteFindCommand.hpp>


namespace MongoCommands
{
	MongoCompleteFindCommand::MongoCompleteFindCommand(std::shared_ptr<MongoCommands::MongoConnection> connection,
		std::shared_ptr<MongoCommands::MongoFindCommand> find_command)
		: connection(connection), find_command(find_command)
	{
		this->connection = connection;
	}


	void MongoCompleteFindCommand::init()
	{
		this->find_command->init();
	}


	void MongoCompleteFindCommand::process()
	{
		this->find_command->process();
	}


	void MongoCompleteFindCommand::end()
	{
		this->find_command->end();
	}
}