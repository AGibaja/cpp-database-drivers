#include <MongoFindCommand.hpp>

#include <MongoConnection.hpp>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/exception/exception.hpp>


using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;


namespace MongoCommands
{
	MongoFindCommand::MongoFindCommand(std::shared_ptr<MongoConnection> connection,
		std::string collection, std::string command)
		: MongoCommand(connection, collection), command(command)
	{
		this->command_type = Types::CommandType::READ;
		this->process();
	}


	MongoFindCommand::~MongoFindCommand()
	{
	}


	void MongoFindCommand::init()
	{
		MongoCommand::init();
	}


	void MongoFindCommand::process()
	{
		try
		{
			bsoncxx::stdx::string_view str_view{ this->command };
			auto json = bsoncxx::from_json(str_view);
			this->cursor = std::make_shared <mongocxx::cursor>(this->mongo_collection.find({ json }));
		}
		
		catch (bsoncxx::exception &e)
		{
			std::cout << e.what() << std::endl; 
		}
			
	}


	void MongoFindCommand::end()
	{
		
	}


	std::shared_ptr<mongocxx::cursor> MongoFindCommand::getCursor()
	{
		return this->cursor;
	}
}
