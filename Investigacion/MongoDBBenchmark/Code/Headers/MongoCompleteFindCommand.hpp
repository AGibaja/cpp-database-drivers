/*!
@class MongoCompleteFindCommand
@brief Class that holds a MongoConnectiion and a MongoCommand.
*/

#ifndef MONGOCOMPLETEFINDCOMMAND_HPP
#define MONGOCOMPLETEFINDCOMMAND_HPP

#include <MongoConnection.hpp>
#include <MongoFindCommand.hpp>


namespace MongoCommands
{
	class MongoCompleteFindCommand : public BenchmarkCore::Command
	{
	public:
		MongoCompleteFindCommand(std::shared_ptr<MongoCommands::MongoConnection> connection,
			std::shared_ptr<MongoCommands::MongoFindCommand> find_command);

		///Initialize the command calling its contained MongoFindCommand init()
		void init() override;
		///Process the command calling its contained MongoFindCommand init()
		void process() override;
		///End the command calling its contained MongoFindCommand init()
		void end() override;


	private:
		///Connection that contains the data and actions needed to connect to the 
		///mongo server.
		std::shared_ptr<MongoCommands::MongoConnection> connection = nullptr;

		///Find command that contains the commands and necessary info to execute them.
		std::shared_ptr<MongoCommands::MongoFindCommand> find_command = nullptr;


	};
}


#endif