#ifndef MONGOCONNECTION_HPP
#define MONGOCONNECTION_HPP

#include <iostream>

#include <ConnectionInfo.hpp>

#include <mongocxx/database.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/client.hpp>


using namespace Network;


namespace MongoCommands
{
	class MongoConnection : public ConnectionInfo
	{
	public:
		MongoConnection(Network::ConnectionInfo::ConnectionParams &params);
		MongoConnection(std::string host = "127.0.0.1", std::string port = "27017", std::string db_name = "root",
			std::string user = "root", std::string password = "root");
		~MongoConnection();


		void initConnection() override;
		void makeConnection() override;
		void endConnection() override;

		void setMongoDatabase(mongocxx::database db);

		bool connectionInited();

		std::string &getBuiltConnection(); 
		std::string &getPort(); 

		mongocxx::database getMongoDatabase();


	private:
		bool inited = false; 

		std::string built_connection = "mongodb://";
		std::string port = "27017"; 

		mongocxx::instance instance;
		mongocxx::database database;
		mongocxx::client client;
		

	};


}


#endif // !define MONGOCONNECTION_HPP
