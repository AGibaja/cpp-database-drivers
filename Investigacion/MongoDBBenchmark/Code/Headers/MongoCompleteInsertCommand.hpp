#pragma once
#ifndef MONGOCOMPLETEINSERTCOMMAND_HPP
#define MONGOCOMPLETEINSERTCOMMAND_HPP

#include <MongoConnection.hpp>
#include <MongoInsertCommand.hpp>


namespace MongoCommands
{
	class MongoCompleteInsertCommand : public BenchmarkCore::Command
	{
	public:
		MongoCompleteInsertCommand(std::shared_ptr<MongoConnection> connection,
			std::shared_ptr<MongoCommands::MongoInsertCommand> command);

		void init() override;
		void process() override;
		void end() override;

		std::shared_ptr<MongoConnection> getConnection();

		std::shared_ptr<MongoCommands::MongoInsertCommand> getInsertCommand();


	private:
		std::shared_ptr<MongoConnection> connection = nullptr;

		std::shared_ptr<MongoInsertCommand> command = nullptr;


	};

}

#endif