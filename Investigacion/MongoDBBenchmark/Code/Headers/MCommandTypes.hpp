#ifndef MCOMMANDTYPES_HPP
#define MCOMMANDTYPES_HPP

namespace MongoCommandTypes
{
	enum MongoReadCommandTypes
	{
		UNDEFINED = 0,
		FIND
	};
}

#endif // !define MCOMMANDTYPES_HPP
