#ifndef MONGOCOMMAND_HPP
#define MONGOCOMMAND_HPP

#include <memory>

#include <Command.hpp>

#include <mongocxx/collection.hpp>


namespace MongoCommands 
{
	class MongoConnection;

	class MongoCommand : public BenchmarkCore::Command
	{
	public:
		MongoCommand(std::shared_ptr<MongoConnection> mongo_conn, std::string collection_name);
		~MongoCommand();

		virtual void init() override; 
		virtual void process() override; 
		virtual void end() override; 

		std::shared_ptr<MongoConnection> getConnection();
		

	protected:
		mongocxx::collection mongo_collection;

		std::string collection_name;


	private:
		std::shared_ptr <MongoConnection> parent_connection
			= nullptr;


	};

}


#endif // !MONGOCOMMAND_HPP
