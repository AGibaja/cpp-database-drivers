#ifndef MONGOWRITECOMMAND_HPP
#define MONGOWRITECOMMAND_HPP

#include <MongoCommand.hpp>

#include <memory>
#include <string>

#include <bsoncxx/document/value.hpp>



namespace MongoCommands
{
	class MongoInsertCommand : public MongoCommand
	{
	public:
		MongoInsertCommand(std::shared_ptr<MongoConnection> connection, 
			std::string collection, std::string data_to_insert);
		~MongoInsertCommand();


		void init() override;
		void process() override;
		void end() override;

	private:
		std::string data_to_insert;

		std::vector<bsoncxx::document::value> docs;

	};
}


#endif // !define MONGOWRITECOMMAND_HPP
