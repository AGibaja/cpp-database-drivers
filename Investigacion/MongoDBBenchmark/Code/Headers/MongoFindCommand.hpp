#ifndef MONGOFINDCOMMAND_HPP
#define MONGOFINDCOMMAND_HPP

#include <MongoCommand.hpp>

#include <memory>
#include <string>

#include <mongocxx/client.hpp>


namespace MongoCommands
{

	class MongoFindCommand : public MongoCommand
	{
	public:
		MongoFindCommand(std::shared_ptr<MongoConnection> connection, 
			std::string collection,
			std::string document_to_look);
		~MongoFindCommand();


		void init() override;
		void process() override;
		void end() override;


		std::shared_ptr<mongocxx::cursor> getCursor();


	private:
		std::string command = "{}";


		std::shared_ptr<mongocxx::cursor> cursor = nullptr;


	};
}

#endif // ! MONGOFINDCOMMAND_HPP
