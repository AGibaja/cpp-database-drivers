#include <Chart2D.hpp>


Chart2D::Chart2D(QWidget *parent)
	: QWidget (parent)
{

}


Chart2D::Chart2D(std::map<int, int> &values, QString title, QWidget *parent)
	: QWidget(parent), title(title), values(values)
{
	this->initLayout();
	this->initLabel();
	this->initChart();
	this->setLayout(this->vertical_layout);
}


Chart2D::~Chart2D()
{
}


void Chart2D::initAll()
{
	this->initLayout();
	this->initLabel();
	this->initChart();
	this->setLayout(this->vertical_layout);
}


void Chart2D::setValues(std::map<int, int>& vals)
{
	this->values = vals;
}


void Chart2D::setTitle(QString & title)
{
	this->title = title;
}


QChartView * Chart2D::getChartView()
{
	return this->chart_view;
}


QChart * Chart2D::getChart()
{
	return this->chart;
}


QValueAxis * Chart2D::getXAxis()
{
	return this->x_axis;
}


QValueAxis * Chart2D::getYAxis()
{
	return this->y_axis;
}


QLineSeries * Chart2D::getLineSeries()
{
	return this->line_series;
}


void Chart2D::initLayout()
{
	this->vertical_layout = new QVBoxLayout();
}


void Chart2D::initLabel()
{
	this->label = new QLabel(this->title);
	this->label->setParent(this);
	this->label->setAlignment(Qt::AlignCenter);
	this->vertical_layout->addWidget(this->label);
}


void Chart2D::initChart()
{
	this->chart_view = new QChartView(this);
	this->chart_view->setRenderHint(QPainter::Antialiasing);

	this->chart = new QChart();
	this->chart_view->setChart(this->chart);

	this->x_axis = new QValueAxis();
	this->chart->addAxis(x_axis, Qt::AlignBottom);

	this->y_axis = new QValueAxis();
	this->chart->addAxis(y_axis, Qt::AlignLeft);

	this->line_series = new QLineSeries();
	for (auto & e : this->values)
		this->line_series->append(e.first, e.second);

	this->line_series->setPointsVisible(true);
	
	this->chart->addSeries(this->line_series);

	this->line_series->attachAxis(this->x_axis);
	this->line_series->attachAxis(this->y_axis);

	this->vertical_layout->addWidget(this->chart_view);
}
