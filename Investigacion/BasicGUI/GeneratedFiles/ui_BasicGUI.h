/********************************************************************************
** Form generated from reading UI file 'BasicGUI.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BASICGUI_H
#define UI_BASICGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BasicGUIClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *BasicGUIClass)
    {
        if (BasicGUIClass->objectName().isEmpty())
            BasicGUIClass->setObjectName(QString::fromUtf8("BasicGUIClass"));
        BasicGUIClass->resize(600, 400);
        menuBar = new QMenuBar(BasicGUIClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        BasicGUIClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(BasicGUIClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        BasicGUIClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(BasicGUIClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        BasicGUIClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(BasicGUIClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        BasicGUIClass->setStatusBar(statusBar);

        retranslateUi(BasicGUIClass);

        QMetaObject::connectSlotsByName(BasicGUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *BasicGUIClass)
    {
        BasicGUIClass->setWindowTitle(QCoreApplication::translate("BasicGUIClass", "BasicGUI", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BasicGUIClass: public Ui_BasicGUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BASICGUI_H
