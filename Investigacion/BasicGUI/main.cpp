#include <MainWindow.hpp>
#include <QtWidgets/QApplication>

#include <qchartview.h>
#include <qdatetimeaxis.h>
#include <qvalueaxis.h>
#include <qlineseries.h>
#include <qpixmap.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qopenglwidget.h>
#include <qpoint.h>


#include <IntroWindow.hpp>


using namespace QtCharts;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	IntroWindow *window = new IntroWindow();
	window->show(); 

	return a.exec();
}



/*
QLabel *mw = new QLabel();
	mw->resize(500, 500);
	QChartView* chartView = new QChartView(mw);
	chartView->setRenderHint(QPainter::Antialiasing);
	chartView->resize(500, 500);

	QChart* chart = new QChart;
	chartView->setChart(chart);

	QValueAxis* xAxis = new QValueAxis;
	chart->addAxis(xAxis, Qt::AlignBottom);

	QValueAxis* yAxis = new QValueAxis;
	chart->addAxis(yAxis, Qt::AlignLeft);

	QLineSeries* series = new QLineSeries;
	for (int i = 0; i < 10; ++i)
	{
		series->append(i,i);
	}
	series->setPointsVisible(true);

	chart->addSeries(series);
	series->attachAxis(xAxis);
	series->attachAxis(yAxis);


*/