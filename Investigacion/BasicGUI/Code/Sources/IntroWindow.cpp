#include <IntroWindow.hpp>

#include <iostream>

#include <QPushButton>
#include <QLineEdit>
#include <QFileDialog>
#include <QMessageBox>

#include <Chart2D.hpp>


using namespace OwnParser;


IntroWindow::IntroWindow(QSize size, QWidget *parent)
	: QFrame(parent)
{
	this->resize(size);
	this->init();
}


IntroWindow::~IntroWindow()
{
}


void IntroWindow::init()
{
	QFrame *fr = new QFrame(this);
	fr->resize(this->size());

	this->main_layout = new QVBoxLayout();
	this->main_layout->setAlignment(Qt::AlignCenter | Qt::AlignBottom);

	this->horizontal_layout = new QHBoxLayout();

	this->line_edit = new QLineEdit(); 
	line_edit->setReadOnly(true); 

	QPushButton *browse = new QPushButton("Browse");
	connect(browse, SIGNAL(clicked()), this, SLOT(generateFileExplorer()));
	QPushButton *generate = new QPushButton("Generate");
	connect(generate, SIGNAL(clicked()), this, SLOT(createChart()));

	this->chart_2d = new Chart2D();
	this->chart_2d->setTitle(QString("No chart selected"));
	this->chart_2d->initAll();

	this->horizontal_layout->addWidget(line_edit);
	this->horizontal_layout->addWidget(browse);
	this->horizontal_layout->addWidget(generate);

	this->main_layout->addWidget(this->chart_2d);
	this->main_layout->addLayout(this->horizontal_layout);
	
	fr->setLayout(this->main_layout);
}


void IntroWindow::generateFileExplorer()
{
	this->file_dialog = new QFileDialog(); 
	connect(*&file_dialog, SIGNAL(fileSelected(QString)), this, SLOT(printSelectedFile()));
	this->file_dialog->exec(); 
}


void IntroWindow::printSelectedFile()
{
	if (this->file_dialog->selectedFiles().count() > 1)
	{
		QMessageBox m;
		m.setText("Only one document is accepted.");
		m.show();
	}
	else
	{
		this->path_to_file = this->file_dialog->selectedFiles()[0];
		this->line_edit->setText(path_to_file);
	}
}


void IntroWindow::createChart()
{
	this->chart_2d->deleteLater();
	this->parser = std::make_shared<StatsParser>(this->path_to_file.toStdString());

	for (auto &e : this->parser->getXYMap())
	{
		std::cout << e.first << e.second << std::endl;
	}

	this->chart_2d = new Chart2D(this->parser->getXYMap(), this->parser->getTitle());
	this->main_layout->insertWidget(0, this->chart_2d);
	this->chart_2d->show(); 
}

