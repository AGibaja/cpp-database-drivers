#ifndef INTROWINDOW_HPP
#define INTROWINDOW_HPP

#include <memory>

#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <StatsParser.hpp>


class QFileDialog;
class Chart2D;


class IntroWindow : public QFrame
{
	Q_OBJECT
public:
	IntroWindow(QSize size = QSize(500, 500), QWidget *parent = nullptr);
	~IntroWindow();


private:
	QString path_to_file;

	QStringList str_list;

	QVBoxLayout *main_layout = nullptr;

	QHBoxLayout *horizontal_layout = nullptr;

	QFileDialog *file_dialog = nullptr; 

	QLineEdit *line_edit = nullptr;

	Chart2D *chart_2d = nullptr;

	std::shared_ptr<OwnParser::StatsParser> parser = nullptr;


	void init();


public slots:
	void generateFileExplorer();
	void printSelectedFile();
	void createChart();


};

#endif // !define INTROWINDOW_HPP
