#ifndef OWNCHART_HPP
#define OWNCHART_HPP

#include <map>

#include <qchartview.h>
#include <qdatetimeaxis.h>
#include <qvalueaxis.h>
#include <qlineseries.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qstring.h>


using namespace QtCharts;

class Chart2D : public QWidget
{
Q_OBJECT

public:
	Chart2D(QWidget *parent = nullptr);
	Chart2D(std::map<int, int> &values, QString title = "2D Chart", QWidget *parent = nullptr);
	~Chart2D();

	void initAll();
	void setValues(std::map <int, int> &vals);
	void setTitle(QString &title);

	QChartView *getChartView();

	QChart *getChart();

	QValueAxis *getXAxis();
	QValueAxis *getYAxis();

	QLineSeries *getLineSeries();


private:
	QString title;

	std::map <int, int> values; 

	QVBoxLayout *vertical_layout = nullptr;

	QLabel *label = nullptr;

	QChartView *chart_view = nullptr;

	QChart *chart = nullptr;

	QValueAxis *x_axis = nullptr;
	QValueAxis *y_axis = nullptr;

	QLineSeries *line_series = nullptr;

	void initLayout();
	void initLabel();
	void initChart();

};

#endif
