#include <iostream>
#include <string>
#include <map>

#include <FileReader.hpp>
#include <Stats.hpp>
#include <FileWriter.hpp>

using namespace OwnIO;


int main()
{
	system("pwd");
	FileWriter fr("test.json", true);

	std::map<int, int> vals;

	for (int i = 0; i < 10; ++i)
	{
		vals[i] = i;
	}

	Stats st("PostgresqlBenchmark", "XAxis", "YAxis", vals);
	std::cout << st.getAsString() << std::endl;


	std::string to_write = st.getAsString();
	fr.writeString(to_write);

	std::cin.get();
}