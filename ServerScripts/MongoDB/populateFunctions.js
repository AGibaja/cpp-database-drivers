const Direction = {"IN":"IN", "OUT":"OUT"};
Object.freeze(Direction);


class Entry
{
    constructor(dir_type, timestamp, waypoint_id, camera_id, area_id, center_id)
    {
        this.direction = dir_type;
        this.timestamp = timestamp;
        this.waypoint_id = waypoint_id; 
        this.camera_id = camera_id; 
        this.area_id = area_id; 
        this.center_id = center_id;
        this.number = 0;
    }
}


function ackLoaded()
{
    print ("Populate functions script loaded. Invoke helpPopulate() for more info.");
}


function helpPopulate()
{
    print ("populateDocument function usage: \n"
    + "   type insertEntries(int number_entries, string name_collection) to insert *number_entries* entries\n"
    + "   into a collection with name *name_collection*. Inserted documents will have the following structure:\n"
    + "   --> Direction : can be IN or OUT."
    + "   --> Timestamp : timestamp of the entry, in this case it's the date when the function has been called\n"
    + "       in UTC timezone. Format is: yyyy-mm-dd-mm-ss (check if this is correct.)" 
    + "   --> Waypoint_id : id of the fake waypoint that it belongs to."
    + "   --> Camera_id : id of the fake camera that it belongs to."
    + "   --> Area_id : id of the fake area that it belongs to."
    + "   --> Center_id : id of the fake center that it belongs to.");

}


function insertEntries(count, collection_name)
{
    let coll = db.getCollection(collection_name).exists();
    if (coll == null)
        print("Error, collection with name: "+ collection_name + " doesn't exist.");
    else 
    {
        for (let i = 0; i < count; ++i)
        {
            if (i % 2 == 0)
            {
                let entry = new Entry(Direction.IN, new Date().toISOString(),
                    "dummy_center_id", "dummy_camera_id", "dummy_area_id", "dummy_center_id"); 
                db.getCollection(collection_name).insert(entry);
            }
                
            else
            {
                let entry = new Entry(Direction.OUT, new Date().toISOString(),
                    "dummy_odd_center_id", "dummy_odd_camera_id", "dummy_odd_area_id", "dummy_odd_center_id");
                db.getCollection(collection_name).insert(entry);
            }
        }
        print("");
        print(count + " entries inserted.");
        print("");
    }
}

ackLoaded();
